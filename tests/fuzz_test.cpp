#include <gtest/gtest.h>
#include <random>
#include <iostream>

#include "parser.h"


class Fuzzer {
public:
    Fuzzer(uint32_t seed, uint32_t max_size) : gen_(seed), max_size_(max_size) {}

    std::string next() {
        std::uniform_int_distribution<uint32_t> size_gen(1, max_size_);
        std::uniform_int_distribution<char> char_gen(41, 90);

        std::string expression;
        uint32_t size = size_gen(gen_);
        for (int i = 0; i < size; ++i) {
            expression += char_gen(gen_);
        }
        return expression;
    }

private:
    std::mt19937 gen_;
    uint32_t max_size_;
};

TEST(FuzzTest, SimpleFuzz) {
    constexpr uint32_t kShotsCount = 100000;
    constexpr uint32_t kMaxSize = 100;
    constexpr uint32_t kSeed = 16;
    auto fuzzer = Fuzzer(kSeed, kMaxSize);
    for (uint32_t i = 0; i < kShotsCount; ++i) {
        try {
            std::string expression = fuzzer.next();
            std::cout << expression << std::endl;
            auto node = Parser().parse(expression);
            node->eval();
        } catch (const IncorrectExression& ex) {
            continue;
        } catch (const MissingVariableValueError& ex) {
            continue;
        }
    }
}