#include "parser.h"

#include <gtest/gtest.h>

TEST(ParserTest, Constant) {
    auto parser = Parser();
    auto function = parser.parse("1");
    ASSERT_DOUBLE_EQ(1, function->eval());
}

TEST(ParserTest, TemplateType) {
    auto stoi = [](const auto& a) { return std::stoi(a); };
    auto parser = Parser<int>(stoi);
    auto function = parser.parse("1.1");
    ASSERT_DOUBLE_EQ(1, function->eval());
}

TEST(ParserTest, UserDefinedOperators) {
    auto abs = [](double a) { return a < 0 ? -a : a; };
    auto parser = Parser({{"abs", abs}}, {});
    auto function = parser.parse("abs(-1)");
    ASSERT_EQ(1, function->eval({{"a", 1}}));
}

TEST(ParserExceptionTest, ValueNotGiven) {
    auto parser = Parser();
    auto function = parser.parse("x");
    ASSERT_THROW(function->eval(), MissingVariableValueError);
}

TEST(ParserExceptionTest, NoOperation) {
    auto parser = Parser();
    ASSERT_THROW(parser.parse("2 3"), IncorrectExression);
}

TEST(ParserExceptionTest, NotEnoughtOpearnds) {
    auto parser = Parser();
    ASSERT_THROW(parser.parse("2 +"), IncorrectExression);
}

TEST(ParserExceptionTest, Empty) {
    auto parser = Parser();
    ASSERT_THROW(parser.parse(""), IncorrectExression);
}

TEST(ParserExceptionTest, ClosingBracket) {
    auto parser = Parser();
    ASSERT_THROW(parser.parse(")"), IncorrectExression);
}
