#include "tree_builder.h"

#include <gtest/gtest.h>

TEST(TreeBuilderTest, Constant) {
    auto builder = TreeBuilder({"1"});
    auto root = builder.run();
    ASSERT_DOUBLE_EQ(1, root->eval());
}

TEST(TreeBuilderTest, TemplateType) {
    auto stoi = [](const std::string& token) { return std::stoi(token); };
    auto builder = TreeBuilder<int>({"1.1"}, stoi);
    auto root = builder.run();
    ASSERT_EQ(1, root->eval());
}

TEST(TreeBuilderTest, Variables) {
    auto builder = TreeBuilder({"x1"});
    auto root = builder.run();
    ASSERT_DOUBLE_EQ(1, root->eval({{"x1", 1}}));
}

TEST(TreeBuilderTest, UnaryMinus) {
    // -3
    auto builder = TreeBuilder({"3", "unary_minus"});
    auto root = builder.run();
    ASSERT_DOUBLE_EQ(-3, root->eval());
}

TEST(TreeBuilderTest, Minus) {
    // 1 - 2
    auto builder = TreeBuilder({"1", "2", "-"});
    auto root = builder.run();
    ASSERT_DOUBLE_EQ(-1, root->eval());
}

TEST(TreeBuilderTest, Division) {
    auto builder = TreeBuilder({"1", "2", "/"});
    auto root = builder.run();
    ASSERT_DOUBLE_EQ(0.5, root->eval());
}

TEST(TreeBuilderTest, OpsChains) {
    // 1 + 2 - 3
    auto builder = TreeBuilder({"1", "2", "+", "3", "-"});
    auto root = builder.run();
    ASSERT_DOUBLE_EQ(0, root->eval());
}

TEST(TreeBuilderTest, MultipleVariables) {
    // a - b
    auto builder = TreeBuilder({"a", "b", "-"});
    auto root = builder.run();
    ASSERT_DOUBLE_EQ(-1, root->eval({{"a", 1}, {"b", 2}}));
}

TEST(TreeBuilderTest, VariablesWithTemplateType) {
    // a - 1
    auto stoi = [](const std::string& token) { return std::stoi(token); };
    auto builder = TreeBuilder<int>(
        {
            "a",
            "1.1",
            "-",
        },
        stoi);
    auto root = builder.run();
    ASSERT_EQ(0, root->eval({{"a", 1}}));
}

TEST(TreeBuilderTest, UserDefinedUnaryOps) {
    // abs(-1)
    auto abs = [](double a) { return a < 0 ? -a : a; };
    auto builder = TreeBuilder({"1", "unary_minus", "abs"}, {{"abs", abs}}, {});
    auto root = builder.run();
    ASSERT_EQ(1, root->eval({{"a", 1}}));
}

TEST(TreeBuilderTest, UserDefinedBinaryOps) {
    // 1 leet 2 -> 1337 + 1 - 2 = 1336
    auto leet = [](double a, double b) { return 1337 + a - b; };
    auto builder = TreeBuilder({"1", "2", "leet"}, {}, {{"leet", leet}});
    auto root = builder.run();
    ASSERT_EQ(1336, root->eval({{"a", 1}}));
}

TEST(TreeBuilderTest, SeveralRuns) {
    auto builder = TreeBuilder({"1"});
    auto root1 = builder.run();
    auto root2 = builder.run();
    ASSERT_DOUBLE_EQ(1, root2->eval());
}
