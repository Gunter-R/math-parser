#include "token_sorter.h"

#include <gtest/gtest.h>

void sorter_test(std::vector<std::string> tokens, std::vector<std::string> actual_sorted_tokens) {
    auto sorter = TokenSorter(tokens);
    auto sorted_tokens = sorter.run();
    ASSERT_EQ(actual_sorted_tokens.size(), sorted_tokens.size());
    for (int i = 0; i < actual_sorted_tokens.size(); ++i) {
        EXPECT_EQ(actual_sorted_tokens[i], sorted_tokens[i]) << "i = " << i;
    }
}

TEST(TokenSorterTest, EmptyInput) {
    sorter_test({}, {});
}

TEST(TokenSorterTest, Sum) {
    sorter_test({"1", "+", "2"}, {"1", "2", "+"});
}

TEST(TokenSorterTest, LinearOrder) {
    sorter_test({"1", "+", "2", "-", "3"}, {"1", "2", "+", "3", "-"});
}

TEST(TokenSorterTest, Parenthesis) {
    sorter_test({"1", "+", "(", "2", "-", "3", ")"}, {"1", "2", "3", "-", "+"});
}

TEST(TokenSorterTest, OperationPrecedence) {
    sorter_test({"1", "+", "2", "*", "3"}, {"1", "2", "3", "*", "+"});
}

TEST(TokenSorterTest, UnaryMinus) {
    sorter_test({"-", "3", "*", "4"}, {"3", "unary_minus", "4", "*"});
}

TEST(TokenSorterTest, UserDefinedOps) {
    std::vector<std::string> tokens{"1", "+", "2", "lol", "3"};
    std::vector<std::string> actual_sorted_tokens{"1", "2", "3", "lol", "+"};
    auto sorter = TokenSorter(tokens, {"lol"});
    auto sorted_tokens = sorter.run();
    ASSERT_EQ(actual_sorted_tokens.size(), sorted_tokens.size());
    for (int i = 0; i < actual_sorted_tokens.size(); ++i) {
        EXPECT_EQ(actual_sorted_tokens[i], sorted_tokens[i]) << "i = " << i;
    }
}

TEST(TokenSorterTest, SeveralRuns) {
    std::vector<std::string> tokens{"1"};
    std::vector<std::string> actual_sorted_tokens{"1"};
    auto sorter = TokenSorter(tokens);
    auto sorted_tokens1 = sorter.run();
    auto sorted_tokens2 = sorter.run();
    ASSERT_EQ(actual_sorted_tokens.size(), sorted_tokens2.size());
    for (int i = 0; i < actual_sorted_tokens.size(); ++i) {
        EXPECT_EQ(actual_sorted_tokens[i], sorted_tokens1[i]) << "i = " << i;
    }
}
