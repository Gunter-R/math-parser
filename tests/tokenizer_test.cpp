#include "tokenizer.h"

#include <gtest/gtest.h>

#include <string>
#include <vector>

void tokenizer_test(const std::string& expression, const std::vector<std::string>& actual_tokens) {
    auto tokenizer = Tokenizer(expression);
    auto tokens = tokenizer.run();

    ASSERT_EQ(actual_tokens.size(), tokens.size());
    for (int i = 0; i < actual_tokens.size(); ++i) {
        EXPECT_EQ(actual_tokens[i], tokens[i]);
    }
}

TEST(SimpleTokenizerTest, Empty) {
    tokenizer_test("", {});
}

TEST(SimpleTokenizerTest, Constant) {
    tokenizer_test("12", {"12"});
}

TEST(SimpleTokenizerTest, Decimal) {
    tokenizer_test("1.2", {"1.2"});
}

TEST(SimpleTokenizerTest, Whitespace) {
    tokenizer_test(" 1   +  1  ", {"1", "+", "1"});
}

TEST(SimpleTokenizerTest, Parenthesis) {
    tokenizer_test("(1 + 1)", {"(", "1", "+", "1", ")"});
}

TEST(SimpleTokenizerTest, Variable) {
    tokenizer_test("a1 + xyz", {"a1", "+", "xyz"});
}

TEST(SimpleTokenizerTest, SeveralRuns) {
    auto tokenizer = Tokenizer("1");
    auto tokens1 = tokenizer.run();
    auto tokens2 = tokenizer.run();
    ASSERT_EQ(1, tokens2.size());
    ASSERT_EQ("1", tokens2[0]);
}
