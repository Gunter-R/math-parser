# math-parser

![coverage](https://gitlab.com/gunter-cpp/math-parser/badges/main/coverage.svg)
![pipeline](https://gitlab.com/gunter-cpp/math-parser/badges/main/pipeline.svg
)
![license](https://img.shields.io/gitlab/license/gunter-cpp%2Fmath-parser
)

Purely educational project.

Program that is supposed to be able to parse a mathematical function and then evaluate given variable values.