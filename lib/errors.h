#pragma once

#include <stdexcept>

struct MissingVariableValueError : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

struct IncorrectExression : public std::runtime_error {
    using std::runtime_error::runtime_error;
};