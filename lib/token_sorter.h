#pragma once

#include <functional>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

class TokenSorter {
public:
    explicit TokenSorter(const std::vector<std::string>& tokens) : TokenSorter(tokens, {}) {}

    explicit TokenSorter(const std::vector<std::string>& tokens,
                         const std::vector<std::string>& user_operators) {
        add_user_operators(user_operators);
        preprocess(tokens);

    }

    std::vector<std::string> run();

private:
    enum class TokenType {
        kLeftBracket,
        kRightBracket,
        kOperator,
        kOperand,
    };
    struct TokenInfo {
        std::string token;
        TokenType type;
        int priority;
    };

    void add_user_operators(const std::vector<std::string>& user_operators);
    void preprocess(const std::vector<std::string>& tokens);
    void pop_while(const std::function<bool(const TokenInfo&)>& predicate);

    std::vector<TokenInfo> token_infos_;
    std::stack<TokenInfo> stack_;
    std::vector<std::string> sorted_tokens_;
    std::unordered_map<std::string, TokenInfo> known_tokens_ = {
        {"(", {"(", TokenType::kLeftBracket, 0}},
        {")", {")", TokenType::kRightBracket, 0}},
        {"+", {"+", TokenType::kOperator, 0}},
        {"-", {"-", TokenType::kOperator, 0}},
        {"*", {"*", TokenType::kOperator, 1000}},
        {"/", {"/", TokenType::kOperator, 1000}},
        {"unary_minus", {"unary_minus", TokenType::kOperator, 3000}},
    };
};