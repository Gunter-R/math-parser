#pragma once

#include <string>
#include <vector>

class Tokenizer {
public:
    explicit Tokenizer(std::string expression)
        : expression_(std::move(expression)), index_(0), it_(expression_.begin()) {
    }
    std::vector<std::string> run();
    void skip_whitespaces();
    void read_number();
    void read_string();
    void read_operator();

private:
    std::string expression_;
    std::vector<std::string> tokens_;
    size_t index_;
    std::string::const_iterator it_;
};