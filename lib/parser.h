#pragma once

#include "token_sorter.h"
#include "tokenizer.h"
#include "tree_builder.h"

#define STOI [](const auto& token) { return std::stod(token); }

template <class T = double>
class Parser {
public:
    Parser() : Parser(STOI) {
    }
    explicit Parser(std::function<T(const std::string&)> string_to_type)
        : Parser(string_to_type, {}, {}) {
    }
    explicit Parser(const unary_funcs_map<T>& unary_funcs, const binary_funcs_map<T>& binary_funcs)
        : Parser(STOI, unary_funcs, binary_funcs) {
    }
    explicit Parser(std::function<T(const std::string&)> string_to_type,
                    const unary_funcs_map<T>& unary_funcs, const binary_funcs_map<T>& binary_funcs)
        : string_to_type_(string_to_type), unary_funcs_(unary_funcs), binary_funcs_(binary_funcs) {
        for (const auto& [key, value] : unary_funcs) {
            known_tokens_.emplace_back(key);
        }
        for (const auto& [key, value] : binary_funcs) {
            known_tokens_.emplace_back(key);
        }
    }

    std::shared_ptr<Node<T>> parse(const std::string& expression) const {
        auto tokens = Tokenizer(expression).run();
        auto sorted_tokens = TokenSorter(tokens, known_tokens_).run();
        auto tree =
            TreeBuilder<T>(sorted_tokens, string_to_type_, unary_funcs_, binary_funcs_).run();
        return tree;
    }

private:
    std::function<T(const std::string&)> string_to_type_;
    std::vector<std::string> known_tokens_;
    unary_funcs_map<T> unary_funcs_;
    binary_funcs_map<T> binary_funcs_;
};