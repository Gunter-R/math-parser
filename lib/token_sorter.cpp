#include "token_sorter.h"

std::vector<std::string> TokenSorter::run() {
    if (!sorted_tokens_.empty()) {
        return sorted_tokens_;
    }
    for (const auto& token_info : token_infos_) {
        switch (token_info.type) {
            case TokenType::kOperand:
                sorted_tokens_.emplace_back(token_info.token);
                continue;
            case TokenType::kOperator:
                pop_while([&token_info](auto top) {
                    return top.type == TokenType::kOperator && top.priority >= token_info.priority;
                });
                stack_.emplace(token_info);
                break;
            case TokenType::kLeftBracket:
                stack_.emplace(token_info);
                break;
            case TokenType::kRightBracket:
                pop_while([](auto top) { return top.type != TokenType::kLeftBracket; });
                if (!stack_.empty()) {
                    stack_.pop();
                }
                break;
            default:
                break;
        }
    }
    pop_while([](auto) { return true; });
    return sorted_tokens_;
}

void TokenSorter::pop_while(const std::function<bool(const TokenInfo&)>& predicate) {
    while (!stack_.empty() && predicate(stack_.top())) {
        sorted_tokens_.emplace_back(stack_.top().token);
        stack_.pop();
    }
}

void TokenSorter::add_user_operators(const std::vector<std::string>& user_operators) {
    for (const auto& token : user_operators) {
        known_tokens_[token] = {token, TokenType::kOperator, 5000};
    }
}
void TokenSorter::preprocess(const std::vector<std::string>& tokens) {
    token_infos_.reserve(tokens.size());
    for (int i = 0; i < tokens.size(); ++i) {
        if (tokens[i] == "-") {
            if (i == 0) {
                token_infos_.emplace_back(known_tokens_.at("unary_minus"));
                continue;
            }
            if (!known_tokens_.contains(tokens[i - 1])) {
                token_infos_.emplace_back(known_tokens_.at("-"));
                continue;
            }
            if (known_tokens_.at(tokens[i - 1]).type == TokenType::kOperator ||
                known_tokens_.at(tokens[i - 1]).type == TokenType::kLeftBracket) {
                token_infos_.emplace_back(known_tokens_.at("unary_minus"));
            }
        } else {
            if (known_tokens_.contains(tokens[i])) {
                token_infos_.emplace_back(known_tokens_.at(tokens[i]));
            } else {
                token_infos_.emplace_back(tokens[i], TokenType::kOperand, 0);
            }
        }
    }
}
