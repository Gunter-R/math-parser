#pragma once

#include <functional>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

#include "tree.h"

template <class T>
using unary_funcs_map = std::unordered_map<std::string, std::function<T(T)>>;
template <class T>
using binary_funcs_map = std::unordered_map<std::string, std::function<T(T, T)>>;
template <class T>
using convertor = std::function<T(const std::string&)>;

template <class T = double>
class TreeBuilder {
public:
    explicit TreeBuilder(const std::vector<std::string>& sorted_tokens)
        : TreeBuilder(sorted_tokens, [](const auto& token) { return std::stod(token); }) {
    }

    TreeBuilder(const std::vector<std::string>& sorted_tokens, const convertor<T> str_to_type)
        : TreeBuilder(sorted_tokens, str_to_type, {}, {}) {
    }

    explicit TreeBuilder(const std::vector<std::string>& sorted_tokens,
                         const unary_funcs_map<T>& unary_funcs,
                         const binary_funcs_map<T>& binary_funcs)
        : TreeBuilder(
              sorted_tokens, [](const auto& token) { return std::stod(token); }, unary_funcs,
              binary_funcs) {
    }

    TreeBuilder(const std::vector<std::string>& sorted_tokens, const convertor<T> str_to_type,
                const unary_funcs_map<T>& unary_funcs, const binary_funcs_map<T>& binary_funcs)
        : sorted_tokens_(sorted_tokens), str_to_type_(str_to_type) {
        for (auto [key, value] : unary_funcs) {
            unary_funcs_[key] = value;
        }
        for (auto [key, value] : binary_funcs) {
            binary_funcs_[key] = value;
        }
    }

    NodePointer<T> run() const {
        std::stack<NodePointer<T>> stack;
        for (const auto& token : sorted_tokens_) {
            NodePointer<T> new_node;
            if (binary_funcs_.contains(token)) {
                auto func = binary_funcs_.at(token);
                auto operand1 = safe_top_pop(stack);
                auto operand2 = safe_top_pop(stack);
                new_node = std::make_shared<BinaryOp<T>>(operand2, operand1, func);
            } else if (unary_funcs_.contains(token)) {
                auto func = unary_funcs_.at(token);
                auto operand = safe_top_pop(stack);
                new_node = std::make_shared<UnaryOp<T>>(operand, func);
            } else if (std::isdigit(token[0])) {
                new_node = std::make_shared<Constant<T>>(str_to_type_(token));
            } else {
                new_node = std::make_shared<Variable<T>>(token);
            }
            stack.emplace(new_node);
        }
        if (stack.size() != 1) {
            throw IncorrectExression(std::to_string(stack.size()) +
                                     "nodes left in the stack after parsing. Supposed to be 1");
        }
        return stack.top();
    }

private:
    static NodePointer<T> safe_top_pop(std::stack<NodePointer<T>>& stack) {
        if (stack.empty()) {
            throw IncorrectExression("Not enough operands for operator");
        }
        auto top = stack.top();
        stack.pop();
        return top;
    }

    const std::vector<std::string> sorted_tokens_;
    const convertor<T> str_to_type_;
    unary_funcs_map<T> unary_funcs_ = {
        {"unary_minus", [](T value) { return -value; }},
    };
    binary_funcs_map<T> binary_funcs_ = {
        {"-", [](T a, T b) { return a - b; }},
        {"+", [](T a, T b) { return a + b; }},
        {"*", [](T a, T b) { return a * b; }},
        {"/", [](T a, T b) { return a / b; }},
    };
};