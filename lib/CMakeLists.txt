add_library(parser tree_builder.h tokenizer.h tokenizer.cpp
        token_sorter.cpp
        token_sorter.h
        tree.h
        parser.h
        errors.h)

target_include_directories(parser PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})