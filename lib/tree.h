#pragma once

#include <functional>
#include <memory>
#include <unordered_map>
#include <utility>

#include "errors.h"

template <class T>
using variable_values = std::unordered_map<std::string, T>;

template <class T>
class Node : std::enable_shared_from_this<Node<T>> {
public:
    T eval() const {
        return eval({});
    }

    virtual T eval(const variable_values<T>&) const = 0;
    virtual ~Node() = default;
};

template <class T>
class Constant : public Node<T> {
public:
    explicit Constant(T value) : value_(value) {
    }
    T eval(const variable_values<T>& variables) const override {
        return value_;
    }

private:
    T value_;
};

template <class T>
class Variable : public Node<T> {
public:
    explicit Variable(std::string var_name_) : var_name_(std::move(var_name_)) {
    }

    T eval(const variable_values<T>& variables) const override {
        try {
            return variables.at(var_name_);
        } catch (const std::out_of_range&) {
            throw MissingVariableValueError("Value for the variable '" + var_name_ +
                                            "' is not provided to .eval(): ");
        }
    }

private:
    std::string var_name_;
};

template <class T>
using NodePointer = std::shared_ptr<Node<T>>;

template <class T>
class UnaryOp : public Node<T> {
public:
    explicit UnaryOp(NodePointer<T> operand, const std::function<T(T)> operation)
        : operand_(operand), operation_(operation) {
    }

    T eval(const variable_values<T>& variables) const override {
        return operation_(operand_->eval(variables));
    }

private:
    NodePointer<T> operand_;
    const std::function<T(T)> operation_;
};

template <class T>
class BinaryOp : public Node<T> {
public:
    explicit BinaryOp(NodePointer<T> operand1, NodePointer<T> operand2,
                      const std::function<T(T, T)> operation)
        : operand1_(operand1), operand2_(operand2), operation_(operation) {
    }

    T eval(const variable_values<T>& variables) const override {
        return operation_(operand1_->eval(variables), operand2_->eval(variables));
    }

private:
    NodePointer<T> operand1_;
    NodePointer<T> operand2_;
    const std::function<T(T, T)> operation_;
};
