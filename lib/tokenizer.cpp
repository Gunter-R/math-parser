#include "tokenizer.h"

std::vector<std::string> Tokenizer::run() {
    skip_whitespaces();
    while (it_ != expression_.end()) {
        if (std::isdigit(*it_)) {
            read_number();
        } else if (std::isalpha(*it_)) {
            read_string();
        } else {
            read_operator();
        }
        skip_whitespaces();
    }
    return tokens_;
}

void Tokenizer::skip_whitespaces() {
    while (it_ != expression_.end() && std::isspace(*it_)) {
        ++it_;
    }
}
void Tokenizer::read_number() {
    std::string current_token;
    while (it_ != expression_.end() && (std::isdigit(*it_) || *it_ == '.')) {
        current_token += *it_;
        ++it_;
    }
    tokens_.emplace_back(current_token);
}
void Tokenizer::read_string() {
    std::string current_token;
    while (it_ != expression_.end() && (std::isalnum(*it_) || *it_ == '_')) {
        current_token += *it_;
        ++it_;
    }
    tokens_.emplace_back(current_token);
}
void Tokenizer::read_operator() {
    tokens_.emplace_back(1, *it_);
    ++it_;
}
